import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PlayerBooks extends BaseSchema {
  protected tableName = 'player_books'

  public async up () {
    this.schema.table(this.tableName, () => {
      this.schema.dropTable(this.tableName)
    })
  }

  public async down () {
    this.schema.table(this.tableName, () => {
    })
  }
}
