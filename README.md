program ini adalah program berbasis REST-API berisi tentang database Venue / penyewaan tempat olahraga.
pada program ini terdapat tabel "role", role "1" untuk admin, role "2" untuk pemilik suatu venue, dan role "3" untuk user biasa. admin bisa menginputkan tabel role pada swagger bagian roles. setiap role mempunyai hak aksesnya masing-masing.
setelah role sudah terisi maka user yang lain bisa mendaftarkan dirinya ke aplikasi tersebut dengan role / perannya masing-masing
setelah register, maka user akan mendapatkan OTP-Coded yang dikirimkan melalui email. kode itu harus dimasukkan pada otp-verificaion yang terdapat pada swagger bagian otp-verification untuk mendapatkan validasi akun.
untuk menjalankan beberapa fungsi aplikasi, user diharapkan login terlebih dahulu.
pada bagian venue, pemilik venue dan admin dapat melakukan CRUD "Create, Read, Update, Delete" pada tabel Venues
pada bagian field, pemilik venue dan admin dapat melakukan CRUD "Create, Read, Update, Delete" pada tabel Fields
pada bagian Booking, pemilik venue dan admin dapat melakukan CRUD "Create, Read, Update, Delete" pada tabel books
dengan beberapa fungsi ORM Relationship, maka tampilan menjadi lebih informatif karena bisa melakukan penampilan data secara lengkap
serta penggunaan swagger dapat menguji coba aplikasi secara langsung dengan mengakses (url_apliakasi)/docs

Data Author : Adib Bachtiar
Email : Adibbachtiar44@gmail.com
Instansi : UPN "Veteran" Yogyakarta
Kelas Bootcamp : Sanbercode Nodejs Back-end Batch 25
Trainer : Muhamad Abduh
link deploy heroku : https://venues-olahraga.herokuapp.com/
