import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Role from 'App/Models/Role'
import CreateRoleValidator from 'App/Validators/CreateRoleValidator'
export default class RolesController {
    public async index({response}:HttpContextContract){
        let roles = await Role.all()
        return response.status(200).json({message: 'success get Roles', data:roles})
    }
    public async store({request, response}:HttpContextContract){
        try {
            const role = await request.validate(CreateRoleValidator)
            const newRoles = await Role.create(role)
            return response.created({message: 'created! ', data: newRoles})
        } catch (error) {
            return response.unprocessableEntity({errors:error.messages})
        }
    }
}
